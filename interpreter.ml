open Ast
open Error

let not_implemented () = failwith "Not implemented"

let invalid_access (x) = if x == 0 then failwith "This method isn't compatible with boolean type"
                         else if x == 1 then failwith "Boolean operators are not compatible with Unit"
                         else failwith "It's the fault of OCaml !!!" (* never attained *)

(* Les expressions de notre langage calculent des valeurs qui sont soit
   des entiers, soit des booléens, soit la valeur unité qui ne représente
   rien.
 *)
type value =
  Vint of int
  | Vbool of bool
  | Vunit


(* Cette déclaration nous permet de définir des environnements associant
   des valeurs à des chaînes de caractères.

   Le type de ces environnements est noté ['a Env.t], sachant que dans
   dans notre cas le paramètre ['a] sera systématiquement [value].
   Pour manipuler ces environnements, on a notamment les fonctions
   [Env.find : string -> 'a Env.t -> 'a] et
   [Env.add  : string -> 'a -> 'a Env.t -> 'a Env.t],
   ainsi que la constante [Env.empty : 'a Env.t].
   Voir la documentation de la bibliothèque Caml [Map.Make].
 *)
module Env = Map.Make(String)


(* La fonction suivante prend une valeur (de type [value]) et en extrait
   le booléen qu'elle contient.
   Elle déclenche une erreur [Interpretation_error] si elle est utilisée
   sur une valeur non adaptée (comme [Vunit]). L'argument [pos] sert en cas
   d'erreur à indiquer la partie du fichier interprété qui a provoqué l'erreur.

   get_bool : Ast.position -> value -> bool
 *)
let get_bool pos v =
  match v with
  | Vbool b -> b
  | _       -> error Interpretation_error pos

let get_int pos v =
  match v with
  | Vint i -> i
  | _      -> error Interpretation_error pos
(* Voici le cœur de l'interpréteur, qu'il va falloir compléter. Cette fonction
   prend en argument un environnement associant des valeurs à des chaînes de
   caractères ainsi qu'une expression de la forme décrite dans [Ast.mli], et
   renvoie la valeur calculée.

   interpret_expr : value Env.t -> Ast.node_expr -> value
 *)
let rec interpret_expr env e =
  match e.expr with
    (* Voici deux cas en exemple, à vous d'écrire les autres ! *)
    Econst c ->
    begin
      match c with
        Cbool b -> Vbool b
      | Cint i -> Vint i
      | Cunit -> Vunit
    end

  | Eident i -> Env.find i env

  | Eunop (Unot, e)   ->
     let b = get_bool e.pos (interpret_expr env e) in
     Vbool (not b)

  | Eunop (Uminus, e)   ->
     let i = get_int e.pos (interpret_expr env e) in
     Vint (-i)

  | Eprint_newline e ->
     let _ = interpret_expr env e in
     print_newline ();
     Vunit

  | Eprint_int e ->
     let i = get_int e.pos (interpret_expr env e) in
     print_int i;
     Vunit

  | Ebinop ((Badd | Bsub | Bmul | Bdiv) as op, e1, e2) ->
     let i1 = get_int e1.pos (interpret_expr env e1)
       and
         i2 = get_int e2.pos (interpret_expr env e2)
     in
     Vint (match op with
             Badd -> i1 + i2
           | Bsub -> i1 - i2
           | Bmul -> i1 * i2
           | Bdiv -> i1 / i2
           | _    -> assert false
          )

  | Ebinop ((Beq | Bneq | Blt  | Ble  | Bgt  | Bge) as op, e1, e2) ->
     let x = interpret_expr env e1
       and
         y = interpret_expr env e2
     in Vbool (
            begin
              match (x, y) with
              | (Vbool b1, Vbool b2) -> begin match op with
                                                Beq  ->
                                                let bl1 = get_bool e1.pos (interpret_expr env e1)
                                                  and
                                                    bl2 = get_bool e2.pos (interpret_expr env e2)
                                                in bl1 == bl2
                                              | Bneq ->  let bl1 = get_bool e1.pos (interpret_expr env e1)
                                                           and
                                                             bl2 = get_bool e2.pos (interpret_expr env e2)
                                                         in bl1 <> bl2
                                              | _ -> invalid_access (0)
                                        end
              | (Vint i1, Vint i2) -> begin match op with
                                              Beq  ->
                                              let bl1 = get_int e1.pos (interpret_expr env e1)
                                                and
                                                  bl2 = get_int e2.pos (interpret_expr env e2)
                                              in bl1 == bl2
                                            | Bneq ->  let bl1 = get_int e1.pos (interpret_expr env e1)
                                                         and
                                                           bl2 = get_int e2.pos (interpret_expr env e2)
                                                       in bl1 <> bl2
                                            | Blt  -> get_int e1.pos (interpret_expr env e1) <  get_int e2.pos (interpret_expr env e2)
                                            | Ble  -> get_int e1.pos (interpret_expr env e1) <= get_int e2.pos (interpret_expr env e2)
                                            | Bgt  -> get_int e1.pos (interpret_expr env e1) >  get_int e2.pos (interpret_expr env e2)
                                            | Bge  -> get_int e1.pos (interpret_expr env e1) >= get_int e2.pos (interpret_expr env e2)
                                            | _    -> assert false
                                      end
              | _ -> invalid_access (1)
            end
          )
  | Ebinop ((Band | Bor) as op, e1, e2) ->
     let b1 = get_bool e1.pos (interpret_expr env e1)
       and
         b2 = get_bool e2.pos (interpret_expr env e2)
     in
     Vbool (match op with
              Band -> b1 && b2
            | Bor  -> b1 || b2
            | _    -> assert false
           )

  | Eseq le ->
     let rec aux le =
       match le with
         [] -> failwith "Empty sequence" (* ne devrait jamais etre atteint *)
       | [e] -> interpret_expr env e (* on interprete la derniere valeur *)
       | e::t -> let _ = interpret_expr env e in aux t
     in aux le

  (* WARNING :: Le dernier ne passe pas selon la machine -> probleme de && paresseux *)
  | Eif (i, th, el) ->
     (*let exp = get_bool i.pos (interpret_expr env i)
     match exp with
       true  -> interpret_expr env th
     | false -> interpret_expr env el*)
     if (get_bool i.pos (interpret_expr env i)) then (interpret_expr env th) else (interpret_expr env el)

  | Eletin (i, eI, e) ->
     let v = interpret_expr env eI in interpret_expr (Env.add i v env) e

(* Enfin, la fonction principale, qu'il faut aussi compléter, et qui doit
   appliquer la fonction d'interprétation des expressions à toutes les
   instructions du programme. N'oubliez pas que les instructions peuvent
   agir sur l'environnement !

   interpret_prog : Ast.prog -> unit
 *)
let interpret_prog (p : Ast.prog) : unit =
  let rec i_rec env p_ =
    match p_ with
    | [] -> print_newline ()

    | Icompute h :: t ->
       let _ = interpret_expr env h in
       i_rec env t

    | (Ilet (n, exp))::list ->
       let v = interpret_expr env exp in let newEnv = (Env.add n v env) in
                                         i_rec newEnv list
  in
  i_rec (Env.empty) p
;;

